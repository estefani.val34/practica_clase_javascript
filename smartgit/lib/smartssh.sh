#!/bin/bash
"${SMARTGIT_JAVA_HOME}/bin/java" "-XX:ErrorFile=${JAVA_ERROR_FILE}" -Xms1m -Xmx64m -cp "${SMARTGIT_CLASSPATH}" -Dsmartgit.logging=false -Djava.net.preferIPv4Stack=true com.syntevo.dvcs.transport.ssh.SdSshMain "$@"
exit 0
