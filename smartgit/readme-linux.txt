Installation
============
Since version 18.2 SmartGit requires a 64-bit Linux system.

SmartGit does not need to be installed; just unpack it to your preferred
location and launch the bin/smartgit.sh script. But you will need a Git
installation. On Ubuntu/Debian-based systems following command should
install it:

 $ sudo apt install git-core

On Fedora or CentOS following should do

 $ sudo dnf install git-core


Menu Item
---------
To create a menu item launch bin/add-menuitem.sh, to remove it later use
bin/remove-menuitem.sh.


Crash Selecting a Certain File (or opening the Log)
===================================================
If Java crashes reproducible selecting a certain file, this most likely is
caused by a bug in the Java Runtime Environment related to regular expressions.
To workaround, please set textEditors.syntaxHighlighting to false in the
preferences, page "Low-level Properties" and restart.


If you have further questions regarding the SmartGit on Linux, please ask in
our SmartGit mailing list:

http://www.syntevo.com/contact/
 
--
Your SyntEvo-team
www.syntevo.com/smartgit
