/**
 *
 *
 * @class Person
 * Class representing a person
 */

class Person{

    // Properties definition
    _DNI;
    _name;
    _surname;
    _phone;
    _birthdate;

    /**
     *Creates an instance of Person.
    * @param {String} pDNI
    * @param {String} pName
    * @param {String} pSurname
    * @param {String} pPhone
    * @param {Date} pBirthdate
    * @memberof Person
    */
    constructor(pDNI, pName, pSurname, pPhone, pBirthdate){
        this._DNI = pDNI;
        this._name = pName;
        this._surname = pSurname;
        this._phone = pPhone;
        this._birthdate = pBirthdate;
    }

/**
 *
 * @return {String}: DNI of the person
 * @memberof Person
 */
get DNI(){
        return this._DNI;
    }
/**
 *
 * @param {String} pDNI: DNI of the person 
 * @memberof Person
 */
set DNI(pDNI){
        this._DNI = pDNI;
    }

    get Name(){
        return this._name;
    }

    set Name(pName){
        this._Name = pName;
    }

    get Surname(){
        return this._surname;
    }

    set Surname(pSurname){
        this._surname = pSurname;
    }

    get Phone(){
        return this._phone;
    }

    set Phone(pPhone){
        this._phone = pPhone;
    }

    get Birthdate(){
        return this._birthdate;
    }

    set Birthdate(pBirthdate){
        this._birthdate = pBirthdate;
    }

/**
 * @name: validateDNI
 * @description: Method that validates the correctness of a DNI.
 * @author: Marisa González
 * @version: 1.0
 * @date 27/11/2019
 * @returns {String}: Message about the DNI validation
 * @memberof Person
 */
validateDNI() {
        var dni = this._DNI;
        var numero
        var letr
        var letra
        var expresion_regular_dni
       
        expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
       
        if(expresion_regular_dni.test (dni) == true){
           numero = dni.substr(0,dni.length-1);
           letr = dni.substr(dni.length-1,1);
           numero = numero % 23;
           letra='TRWAGMYFPDXBNJZSQVHLCKET';
           letra=letra.substring(numero,numero+1);
          if (letra!=letr.toUpperCase()) {
             return 'Dni erroneo, la letra del NIF no se corresponde';
           }else{
             return 'Dni correcto';
           }
        }else{
           return 'Dni erroneo, formato no válido';
         }
      }
}