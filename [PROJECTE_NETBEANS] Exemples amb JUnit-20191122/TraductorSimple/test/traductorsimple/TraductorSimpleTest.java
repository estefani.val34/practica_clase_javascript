/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traductorsimple;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alumne
 */
public class TraductorSimpleTest {
    
    public TraductorSimpleTest() {
    }
    
 
    @Test
    public void anyadirPalabraTest(){
        
        Traductor trad = new Traductor();
        trad.anyadirPalabra("hola","hello");
        assertTrue(trad.traducirPalabra("hola").equals("hello"));
        
    }
    
    @Test
    public void anyadirVariasPalabrasTest(){
        
        Traductor trad = new Traductor();
        trad.anyadirPalabra("hola","hello");
        trad.anyadirPalabra("perro","dog");
        trad.anyadirPalabra("gato","cat");
        assertTrue(trad.traducirPalabra("hola").equals("hello"));
        assertTrue(trad.traducirPalabra("perro").equals("dog"));
        assertTrue(trad.traducirPalabra("gato").equals("cat"));
        
    }
   
}
