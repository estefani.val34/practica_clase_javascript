/*  
@name= index.js
@author= Estefani Paredes Valera
@version= 1.0
@description= Controler of with index.html
@date = 24-11-2019
@params= none 
@return = none 
*/


/*  
@name= $(document).ready
@author= Estefani Paredes Valera
@version= 1.0
@description= Activate events when onload main page
@date = 24-11-2019
@params= none 
@return = none 
*/
$(document).ready(function () {
    $("#inputText").val("");
    $("#container2").hide();
    $("#error").hide();
    $("#no_error").hide();

    var idImg;

    /**
     * @name=  $(".chr").hover
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description=  Resize the image if I place the mouse on top of the image
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $(".chr").hover(function () {
        $(this).css("transform", "scale(1.2)");
    }, function () {
        $(this).css("transform", "scale(1)");
    });


    /**
     * @name= $(".chr").click
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description=  Show  me div 2  where I can put DNA code,  when I click in to a picture
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $(".chr").click(function (event) {
        $("#container1").hide();
        $("#container2").show();
        idImg = $(this).attr('id');
        $(".titlenavbar2").attr("id", idImg);
        $(".titlenavbar2").append('<h4 class="navbar2" id="' + idImg + '" > DNA Analytic for chrmomosome number ' + idImg + '</h4>');
    });


    /**
     * @name= $("#buttonBack").click
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description=  Return to the content of main page when I click button back 
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $("#buttonBack").click(function () {
        $("#container2").hide();
        $("#container1").show();
        $(".navbar2").empty();
        $("#inputText").val("");
        $(".messageerror").removeAttr("style").hide();
        $(".valid").removeClass("valid");
        $(".invalid").removeClass("invalid");
    });
   

     /**
     * @name=   $('#inputText').on
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description= Validate input, if is a empty string or nitrogen bases and add valid or invalid class
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $('#inputText').on('input ', function () {
        var inputtxt = $(this);
        var txt = inputtxt.val();
        var is_no_empty = txt;
        var re = /^[AGCTagct\n]+$/;
        var is_adn = re.test(txt);
        if (is_no_empty && is_adn) {
            inputtxt.removeClass("invalid").addClass("valid");
            $("#error").hide();
            $("#no_error").show();
        }
        else {
            inputtxt.removeClass("valid").addClass("invalid");
            $("#no_error").hide();
            $("#error").show();
        }
    });

    
    /**
     * @name= $("#buttonRegisterAnalysis").click
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description=  Open PopUp Window when I clik the button Register Analysis
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $("#buttonRegisterAnalysis").click(function () {
        var valid = $("#inputText").hasClass("valid");
        if (valid) {
            var decision = confirm("Do you really want to introduce this data?");
            if (decision) {
                window.open("../Practica1UF2/popUpWindows/popUpWindow.html", "blank", "width=400px, height=400px");
            }
        } else {
            alert("ENTER A CORRECT CHAIN PLEASE");
        }
    });

});

